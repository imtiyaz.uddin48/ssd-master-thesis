'''
Evaluate trained model
Master thesis project Cybercom
Roger Kalliomäki
'''

from keras import backend as K
from keras.models import load_model
from keras.optimizers import Adam, SGD
import time

from models.keras_ssd300 import ssd_300
from keras_loss_function.keras_ssd_loss import SSDLoss
from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_L2Normalization import L2Normalization
from data_generator.object_detection_2d_data_generator import DataGenerator
from eval_utils.average_precision_evaluator import Evaluator



IMG_HEIGHT = 300
IMG_WIDTH = 300
NR_CLASSES = 1
DATASET = '<DATASET NAME>'
USE_PNG = False
MODEL = '<MODEL NAME>'
MODEL_MODE = 'inference'
SAVED_MODEL = '<PATH NAME IN SAVED MODELS FOLDER>'
OPTIMIZER = 'Adam'
print(SAVED_MODEL)

# The per-channel mean of the images in the dataset. 
#VOC 2007 [113, 107,  99]
#VOC 2012 [114, 109, 101]
#COCO [123, 117, 104]
#BBD100K [70, 74, 74]
#FCAV [107, 104, 99]
#KITTI [93, 98, 95]
MEAN_COLOR = [107, 104, 99]

# Matching IoU Threshold
APIoU = 0.5

# Set the path to file with results
result_file_path = './results/' + DATASET + '/' + MODEL + '_' + time.strftime('%Y%m%d-%H%M%S') + '.txt'

# Set the paths to the dataset.
images_dir = '/home/rogerkalliomaki/datasets/' + DATASET + '/JPEGImages/'
annotations_dir = '/home/rogerkalliomaki/datasets/' + DATASET + '/Annotations/'
image_set_filename = '/home/rogerkalliomaki/datasets/' + DATASET + '/ImageSets/Main/<EVAL IMAGE SET>'

# Set classes
#classes = ['background', 'car']
#classes = ['background', 'Car']
classes = ['background','aeroplane', 'bicycle', 'bird', 'boat','bottle', 'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog','horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']

# The scales for MS COCO [0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05]
# The scales for PASCAL [0.1, 0.2, 0.37, 0.54, 0.71, 0.88, 1.05]
SCALES = [0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05]


# The anchor box aspect ratios used in the original SSD300 (the order matters)
#ASPECT_RATIOS = [[1.0, 2.0, 0.5],
                 # [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 # [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 # [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 # [1.0, 2.0, 0.5],
                 # [1.0, 2.0, 0.5]]
ASPECT_RATIOS = [[1.0, 2.0, 1.5],
                 [1.0, 2.0, 1.5, 3.0, 0.5],
                 [1.0, 2.0, 1.5, 3.0, 0.5],
                 [1.0, 2.0, 1.5, 3.0, 0.5],
                 [1.0, 2.0, 1.5],
                 [1.0, 2.0, 1.5]]


# Build new model

# Clear previous models from memory.
K.clear_session()

# Set parameters
model = ssd_300(image_size=(IMG_HEIGHT, IMG_WIDTH, 3),
                n_classes=NR_CLASSES,
                mode=MODEL_MODE,
                l2_regularization=0.0005,
                scales=SCALES,
                aspect_ratios_per_layer=ASPECT_RATIOS,
                two_boxes_for_ar1=True,
                steps=[8, 16, 32, 64, 100, 300],
                offsets=[0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                clip_boxes=False,
                variances=[0.1, 0.1, 0.2, 0.2],
                normalize_coords=True,
                subtract_mean=MEAN_COLOR,
                swap_channels=[2, 1, 0],
                confidence_thresh=0.01,
                iou_threshold=0.45,
                top_k=200,
                nms_max_output_size=400)

# Load the trained weights into the model.
weights_path = '../saved_models/' + SAVED_MODEL
model.load_weights(weights_path, by_name=True)

# Compile the model so that Keras won't complain the next time you load it.
if OPTIMIZER == 'Adam':
    optmzr = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
else:
    optmzr = SGD(lr=0.001, momentum=0.9, decay=0.0, nesterov=False)
ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
model.compile(optimizer=optmzr, loss=ssd_loss.compute_loss)






dataset = DataGenerator(load_images_into_memory=False)#, hdf5_dataset_path='../datasets/BDD100K/hdf5/bdd100k.h5')



dataset.parse_xml(images_dirs=[images_dir],
                  image_set_filenames=[image_set_filename],
                  annotations_dirs=[annotations_dir],
                  classes=classes,
                  include_classes='all',
                  exclude_truncated=False,
                  exclude_difficult=False,
                  ret=False,
                  use_png=USE_PNG)



evaluator = Evaluator(model=model,
                      n_classes=NR_CLASSES,
                      data_generator=dataset,
                      model_mode=MODEL_MODE)

results = evaluator(img_height=IMG_HEIGHT,
                    img_width=IMG_WIDTH,
                    batch_size=1,
                    data_generator_mode='resize',
                    round_confidences=False,
                    matching_iou_threshold=APIoU,
                    border_pixels='include',
                    sorting_algorithm='quicksort',
                    average_precision_mode='sample',
                    num_recall_points=11,
                    ignore_neutral_boxes=True,
                    return_precisions=True,
                    return_recalls=True,
                    return_average_precisions=True,
                    verbose=True)

mean_average_precision, average_precisions, precisions, recalls = results


# Save to file
#result_file = open(result_file_path, 'w')

print('\nScores for: ' + MODEL + '\n')

for i in range(1, len(average_precisions)):
    class_result = "{:<14}{:<6}{}".format(classes[i], 'AP', round(average_precisions[i], 3))
    print(class_result)
    #result_file.write(class_result + '\n')
    
total_result = "{:<14}{:<6}{}".format('','mAP', round(mean_average_precision, 3))
#result_file.write('\n' + total_result)
print()
print(total_result)



