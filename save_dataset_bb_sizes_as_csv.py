'''
Save bounding box sizes in dataset in csv file
Master thesis project Cybercom
Roger Kalliomaki
'''

from lxml.etree import parse
from os import listdir
from os.path import isfile, join
import random

# Set dataset, nr of images and the boolean USE_ALL_OBJECTS which defines if
# all objects should be used or only cars.
DATASET = 'BDD100K'
USE_MAX_NR_IMAGES = True
MAX_NR_IMAGES = 5000
USE_ALL_OBJECTS = True

# Set correct annotations path depending on if complete dataset should be used.
if USE_ALL_OBJECTS:
    ANNOTATIONS_PATH = './Annotations_all_classes/'
elif DATASET=='BDD100K':
    ANNOTATIONS_PATH = './Annotations_cars/'
else:
    ANNOTATIONS_PATH = './Annotations/'

# Read xml files and shuffle them.
xml_files = [f for f in listdir(ANNOTATIONS_PATH) if isfile(join(ANNOTATIONS_PATH, f))]
random.shuffle(xml_files)
# Set nr of files to use
nr_files = len(xml_files)
if USE_MAX_NR_IMAGES:
    nr_files = MAX_NR_IMAGES

# Go throug files and save bounding box sizes in a csv file.
if USE_ALL_OBJECTS:
    result_file = open('./'+DATASET+'_all_classes_bb_sizes.csv', 'w')
else:
    result_file = open('./'+DATASET+'_cars_bb_sizes.csv', 'w')
counter = 0
for xml_file_name in xml_files:
    xmlTree = parse(ANNOTATIONS_PATH + xml_file_name)
    for element in xmlTree.findall('object'):
        if USE_ALL_OBJECTS or element.find('name').text == 'car' or element.find('name').text == 'Car':
            xmin = int(element.find('bndbox').find('xmin').text)
            xmax = int(element.find('bndbox').find('xmax').text)
            ymin = int(element.find('bndbox').find('ymin').text)
            ymax = int(element.find('bndbox').find('ymax').text)
            dx = xmax-xmin
            dy = ymax-ymin
            csv_string = '%d,%d\n' % (dx,dy)
            result_file.write(csv_string)
    if USE_MAX_NR_IMAGES and counter == MAX_NR_IMAGES:
        break
    counter += 1
    print('%d/%d' % (counter,nr_files))

	
