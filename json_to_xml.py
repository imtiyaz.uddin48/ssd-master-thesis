'''
Convert JSON annotations to PASCAL xml annotations
Master thesis project Cybercom
Roger Kalliomaki
'''

import argparse
import json
import lxml.etree as ET
import os

# Set classes, xml folder path, imageset file path, JSON
# file path, and boolean SORT_OUT which defines if images
# without annotations should be included
CATEGORIES = ['bus', 'traffic light', 'traffic sign', 'person', 'bike', 'truck', 'motor', 'car', 'train', 'rider']
XML_FOLDER_PATH = "./Annotations/"
IMAGESET_FILE_PATH = "./ImageSets/Main/train.txt"
JSON_FILE_PATH = "./bdd100k_labels_images_train.json"
SORT_OUT = True

# Load JSON file
frames = json.load(open(JSON_FILE_PATH, 'r'))
imageset_file = open(IMAGESET_FILE_PATH, "w")

# Go through all annotations in JSON file
write_to_file = True
for frame in frames:
    if SORT_OUT:
        write_to_file = False
    
    # Set general image information
    annotation = ET.Element("annotation")
    ET.SubElement(annotation, "folder").text = "BDD100K"
    ET.SubElement(annotation, "filename").text = frame['name']
    source = ET.SubElement(annotation, "source")
    ET.SubElement(source, "database").text = "Berkeley Deep Drive Database"
    ET.SubElement(source, "annotation").text = "PASCAL VOC2007"
    ET.SubElement(source, "image").text = "Berkeley Deep Drive Database"
    ET.SubElement(source, "flickrid").text = "Berkeley Deep Drive"
    owner = ET.SubElement(annotation, "owner")
    ET.SubElement(owner, "flickrid").text = "BDD100K"
    ET.SubElement(owner, "name").text = "Berkeley Deep Drive"
    size = ET.SubElement(annotation, "size")
    ET.SubElement(size, "width").text = "1280"
    ET.SubElement(size, "height").text = "720"
    ET.SubElement(size, "depth").text = "3"
    ET.SubElement(annotation, "segmented").text = "0"

    # Go throug all objects in image annotation
    for label in frame['labels']:
        if 'box2d' not in label:
            continue
        xy = label['box2d']
        if xy['x1'] >= xy['x2'] or xy['y1'] >= xy['y2']:
            continue
        if SORT_OUT and label['category'] in CATEGORIES:
            write_to_file = True
        else:
            continue
        # Bounding box coordinates
        x1 = str(int(round(xy['x1'])))
        x2 = str(int(round(xy['x2'])))
        y1 = str(int(round(xy['y1'])))
        y2 = str(int(round(xy['y2'])))
        # General object information
        obj = ET.SubElement(annotation, "object")
        ET.SubElement(obj, "name").text = label['category']
        ET.SubElement(obj, "pose").text = "Unspecified"
        ET.SubElement(obj, "truncated").text = "1" if label['attributes']['truncated'] else "0"
        ET.SubElement(obj, "difficult").text = "0"
        bndbox = ET.SubElement(obj, "bndbox")
        ET.SubElement(bndbox, "xmin").text = x1
        ET.SubElement(bndbox, "ymin").text = y1
        ET.SubElement(bndbox, "xmax").text = x2
        ET.SubElement(bndbox, "ymax").text = y2

    # Write to file
    if write_to_file:
        frame_filename = os.path.splitext(frame['name'])[0]
        file_tree = ET.ElementTree(annotation)
        file_tree.write(XML_FOLDER_PATH + frame_filename + ".xml", pretty_print=True)
        imageset_file.write(frame_filename + "\n")

imageset_file.close()
