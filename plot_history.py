'''
Plot training history
Master thesis project Cybercom
Roger Kalliomaki
'''

from matplotlib import pyplot as plt
import numpy as np

#load history
history = np.genfromtxt('training_log.csv', delimiter=',')

# Plot iterations or epochs
USE_ITERATIONS = True
ITS_PER_EPOCH = 1000
iterations = []
if USE_ITERATIONS:
    for i in range(0, len(history)):
        iterations.append(history[i][0]*ITS_PER_EPOCH)

# Load loss and validation
loss = []
val = []
for i in range(0, len(history)):
    loss.append(history[i][1])
    val.append(history[i][2])

#plot loss curves
plt.figure(figsize=[8,6])
if USE_ITERATIONS:
    plt.plot(iterations,loss,'r',linewidth=3.0)
    plt.plot(iterations,val,'b',linewidth=3.0)
    plt.xlabel('Iterations ',fontsize=16)
else:
    plt.plot(loss,'r',linewidth=3.0)
    plt.plot(val,'b',linewidth=3.0)
    plt.xlabel('Epochs ',fontsize=16)
plt.legend(['Training loss', 'Validation Loss'],fontsize=18)
plt.ylabel('Loss',fontsize=16)
plt.title('Model loss',fontsize=16)
plt.savefig('train_im.png')
plt.show()
