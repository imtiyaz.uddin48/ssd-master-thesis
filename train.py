'''
Train SSD network on dataset
Master thesis project Cybercom
Roger Kalliomäki
'''

from keras import backend as K
from keras.optimizers import Adam, SGD
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, TerminateOnNaN, CSVLogger
from keras.models import load_model
from keras_loss_function.keras_ssd_loss import SSDLoss
from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_L2Normalization import L2Normalization
from models.keras_ssd300 import ssd_300
from ssd_encoder_decoder.ssd_input_encoder import SSDInputEncoder
from data_generator.object_detection_2d_data_generator import DataGenerator
from data_generator.object_detection_2d_geometric_ops import Resize
from data_generator.object_detection_2d_photometric_ops import ConvertTo3Channels
from data_generator.data_augmentation_chain_original_ssd import SSDDataAugmentation
from math import ceil
import os

#----------------------------------- Settings ------------------------------------------------------------

print('Version name')

USE_PRETRAINED_MODEL = False
USE_AUGMENTATION = True
USE_PNG = False
IMG_HEIGHT = 300
IMG_WIDTH = 300
NR_CLASSES = 1
DATASET = '<DATASET NAME>'
MODEL = '<MODEL NAME>'
LOAD_MODEL = '<MODEL NAME>'
MODEL_MODE = 'training'
BATCH_SIZE = 32

INITIAL_EPOCH = 0 # Last computed epoch
FINAL_EPOCH = 80 # Last epoch to compute
# Nr of batch steps in one epoch.
STEPS_PER_EPOCH = 1000

OPTIMIZER = 'SGD'

# Set classes
classes = ['background', 'bus', 'traffic light', 'traffic sign', 'person', 'bike', 'truck', 'motor', 'car', 'train', 'rider']
#classes = ['background', 'car']

# Set the paths to the dataset.
images_dir = '/home/rogerkalliomaki/datasets/' + DATASET + '/JPEGImages/'
annotations_dir = '/home/rogerkalliomaki/datasets/' + DATASET + '/Annotations/'
image_train_set_filename = '/home/rogerkalliomaki/datasets/' + DATASET + '/ImageSets/Main/<TRAIN IMAGE SET>'
image_val_set_filename = '/home/rogerkalliomaki/datasets/' + DATASET + '/ImageSets/Main/<VALIDATION IMAGE SET>'

# Learning rate schedule.
def lr_schedule(epoch):

    if epoch < 50:
        return 0.001
    elif epoch < 90:
        return 0.0001
    else:
        return 0.00001



# The per-channel mean of the images in the dataset. 
#VOC 2007 [113, 107,  99]
#VOC 2012 [114, 109, 101]
#COCO [123, 117, 104]
#BBD100K [70, 74, 74]
#FCAV [107, 104, 99]
#KITTI [93, 98, 95]
MEAN_COLOR = [70, 74, 74]

# The space between two adjacent anchor box center points for each prediction layer.
LAYER_GRID_SIZE = [8, 16, 32, 64, 100, 300]

# The scales for MS COCO [0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05]
# The scales for PASCAL [0.1, 0.2, 0.37, 0.54, 0.71, 0.88, 1.05]
SCALES = [0.05, 0.1, 0.29, 0.48, 0.67, 0.86, 1.05]

# The offsets of the first anchor box center points from the top and left borders of the image as a fraction of the step size for each predictor layer.
OFFSETS = [0.5, 0.5, 0.5, 0.5, 0.5, 0.5]

# The anchor box aspect ratios used in the original SSD300 (the order matters)
#ASPECT_RATIOS = [[1.0, 2.0, 0.5],
                 # [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 # [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 # [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 # [1.0, 2.0, 0.5],
                 # [1.0, 2.0, 0.5]]
# ASPECT_RATIOS = [[1.0, 2.0, 0.5],
                 # [1.0, 2.0, 0.5, 2.5, 1.5],
                 # [1.0, 2.0, 0.5, 2.5, 1.5],
                 # [1.0, 2.0, 0.5, 2.5, 1.5],
                 # [1.0, 2.0, 0.5],
                 # [1.0, 2.0, 0.5]]
# ASPECT_RATIOS = [[1.0, 2.0, 1.5],
                 # [1.0, 2.0, 1.5, 3.0, 0.5],
                 # [1.0, 2.0, 1.5, 3.0, 0.5],
                 # [1.0, 2.0, 1.5, 3.0, 0.5],
                 # [1.0, 2.0, 1.5],
                 # [1.0, 2.0, 1.5]]
ASPECT_RATIOS = [[1.0, 1.5, 0.5],
                 [1.0, 1.5, 0.5, 2.0, 2.5],
                 [1.0, 1.5, 0.5, 2.0, 2.5],
                 [1.0, 1.5, 0.5, 2.0, 2.5],
                 [1.0, 1.5, 0.5],
                 [1.0, 1.5, 0.5]]


#-------------------------------------- Model ------------------------------------------------------------

# Use pre-trained model
if USE_PRETRAINED_MODEL:
    # Path to `.h5` file of the model to be loaded.
    model_path = '<MODEL PATH>'

    # Create an SSDLoss object in order to pass that to the model loader.
    ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)

    K.clear_session() # Clear previous models from memory.

    model = load_model(model_path, custom_objects={'AnchorBoxes': AnchorBoxes,
                                               'L2Normalization': L2Normalization,
                                               'DecodeDetections': DecodeDetections,
                                               'compute_loss': ssd_loss.compute_loss})


# Build new model
else:
    # Clear previous models from memory.
    K.clear_session()

    # Set parameters
    model = ssd_300(image_size=(IMG_HEIGHT, IMG_WIDTH, 3),
                n_classes=NR_CLASSES,
                mode=MODEL_MODE,
                l2_regularization=0.0005,
                scales=SCALES,
                aspect_ratios_per_layer=ASPECT_RATIOS,
                two_boxes_for_ar1=True,
                steps=LAYER_GRID_SIZE,
                offsets=OFFSETS,
                clip_boxes=False, # Whether or not to clip the anchor boxes to lie entirely within the image boundaries
                variances=[0.1, 0.1, 0.2, 0.2], # The variances by which the encoded target coordinates are divided as in the original implementation
                normalize_coords=True,
                subtract_mean=MEAN_COLOR,
                swap_channels=[2, 1, 0])

    # Load the trained weights into the model.
    weights_path = '/home/rogerkalliomaki/saved_models/' + LOAD_MODEL + '.h5'
    model.load_weights(weights_path, by_name=True)

    # Compile the model so that Keras won't complain the next time you load it.
    if OPTIMIZER == 'Adam':
        optmzr = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    else:
        optmzr = SGD(lr=0.001, momentum=0.9, decay=0.0, nesterov=False)
    ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
    model.compile(optimizer=optmzr, loss=ssd_loss.compute_loss)

# Print a summary representation of the SSD model.
model.summary()
	
#-------------------------------------- Data generators---------------------------------------------------

# Instantiate two `DataGenerator` objects: One for training, one for validation.
train_dataset = DataGenerator(load_images_into_memory=False, hdf5_dataset_path=None)
val_dataset = DataGenerator(load_images_into_memory=False, hdf5_dataset_path=None)

train_dataset.parse_xml(images_dirs=[images_dir],
                        image_set_filenames=[image_train_set_filename],
                        annotations_dirs=[annotations_dir],
                        classes=classes,
                        include_classes='all',
                        exclude_truncated=False,
                        exclude_difficult=False,
                        ret=False,
                        use_png=USE_PNG)

val_dataset.parse_xml(images_dirs=[images_dir],
                      image_set_filenames=[image_val_set_filename],
                      annotations_dirs=[annotations_dir],
                      classes=classes,
                      include_classes='all',
                      exclude_truncated=False,
                      exclude_difficult=False,
                      ret=False,
                      use_png=USE_PNG)


# Set the image transformations for pre-processing and data augmentation options.

# For the training generator:
ssd_data_augmentation = SSDDataAugmentation(img_height=IMG_HEIGHT,
                                            img_width=IMG_WIDTH,
                                            background=MEAN_COLOR)

# For the validation generator:
convert_to_3_channels = ConvertTo3Channels()
resize = Resize(height=IMG_HEIGHT, width=IMG_WIDTH)

# Instantiate an encoder that can encode ground truth labels into the format needed by the SSD loss function.

# The encoder constructor needs the spatial dimensions of the model's predictor layers to create the anchor boxes.
predictor_sizes = [model.get_layer('conv4_3_norm_mbox_conf').output_shape[1:3],
                   model.get_layer('fc7_mbox_conf').output_shape[1:3],
                   model.get_layer('conv6_2_mbox_conf').output_shape[1:3],
                   model.get_layer('conv7_2_mbox_conf').output_shape[1:3],
                   model.get_layer('conv8_2_mbox_conf').output_shape[1:3],
                   model.get_layer('conv9_2_mbox_conf').output_shape[1:3]]

ssd_input_encoder = SSDInputEncoder(img_height=IMG_HEIGHT,
                                    img_width=IMG_WIDTH,
                                    n_classes=NR_CLASSES,
                                    predictor_sizes=predictor_sizes,
                                    scales=SCALES,
                                    aspect_ratios_per_layer=ASPECT_RATIOS,
                                    two_boxes_for_ar1=True,
                                    steps=LAYER_GRID_SIZE,
                                    offsets=OFFSETS,
                                    clip_boxes=False,
                                    variances=[0.1, 0.1, 0.2, 0.2],
                                    matching_type='multi',
                                    pos_iou_threshold=0.5,
                                    neg_iou_limit=0.5,
                                    normalize_coords=True)

# Create the generator handles that will be passed to Keras' `fit_generator()` function.
if USE_AUGMENTATION:
    train_augmentation = [ssd_data_augmentation]
else:
    train_augmentation = [convert_to_3_channels, resize]
train_generator = train_dataset.generate(batch_size=BATCH_SIZE,
                                         shuffle=True,
                                         transformations=train_augmentation,
                                         label_encoder=ssd_input_encoder,
                                         returns={'processed_images',
                                                  'encoded_labels'},
                                         keep_images_without_gt=False)

val_generator = val_dataset.generate(batch_size=BATCH_SIZE,
                                     shuffle=False,
                                     transformations=[convert_to_3_channels,
                                                      resize],
                                     label_encoder=ssd_input_encoder,
                                     returns={'processed_images',
                                              'encoded_labels'},
                                     keep_images_without_gt=False)

# Get the number of samples in the training and validations datasets.
train_dataset_size = train_dataset.get_dataset_size()
val_dataset_size   = val_dataset.get_dataset_size()

print("Number of images in the training dataset:\t{:>6}".format(train_dataset_size))
print("Number of images in the validation dataset:\t{:>6}".format(val_dataset_size))


#-------------------------------------- Keras Callbacks---------------------------------------------------

# Set the file path under which you want to save the model.
directory = '/home/rogerkalliomaki/saved_models/' + MODEL + '/'
if not os.path.exists(directory):
    os.makedirs(directory)
model_checkpoint = ModelCheckpoint(filepath= '/home/rogerkalliomaki/saved_models/' + MODEL + '/epoch-{epoch:02d}_loss-{loss:.4f}_val_loss-{val_loss:.4f}.h5',
                                   monitor='val_loss',
                                   verbose=1,
                                   save_best_only=True,
                                   save_weights_only=False,
                                   mode='auto',
                                   period=1)

model_checkpoint_10 = ModelCheckpoint(filepath= '/home/rogerkalliomaki/saved_models/' + MODEL + '/epoch-{epoch:02d}_loss-{loss:.4f}_val_loss-{val_loss:.4f}.h5',
                                   monitor='val_loss',
                                   verbose=1,
                                   save_best_only=False,
                                   save_weights_only=False,
                                   mode='auto',
                                   period=10)

csv_logger = CSVLogger(filename= '/home/rogerkalliomaki/saved_models/' + MODEL + '/training_log.csv',
                       separator=',',
                       append=True)

learning_rate_scheduler = LearningRateScheduler(schedule=lr_schedule,
                                                verbose=1)

terminate_on_nan = TerminateOnNaN()

callbacks = [model_checkpoint,
             model_checkpoint_10,
             csv_logger,
             learning_rate_scheduler,
             terminate_on_nan]


#-------------------------------------------- Training ---------------------------------------------------

history = model.fit_generator(generator=train_generator,
                              steps_per_epoch=STEPS_PER_EPOCH,
                              epochs=FINAL_EPOCH,
                              callbacks=callbacks,
                              validation_data=val_generator,
                              validation_steps=ceil(val_dataset_size/BATCH_SIZE),
                              initial_epoch=INITIAL_EPOCH)
